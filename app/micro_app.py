#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import Flask
from flask import request
from flask import render_template, send_from_directory
# from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
# db = SQLAlchemy(app)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                        'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/hello')
@app.route('/')
def index():
    return 'Hello World'

@app.route('/user/<username>')
def user(username):
    return 'Hello' + username

@app.route('/post/<int:post_id>')
def post(post_id):
    return 'Post #' + str(post_id)

@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        return 'Its POST'
    else:
        return 'Its not POST'

# 5
@app.route('/render')
def render():
    name = u'Ivan (Ваня)'
    return render_template('index_lite.html', n=name)


if __name__ == '__main__':
    app.run(debug=True)


# ------------------------------------------------------------------------------
# ==============================================================================


