#!/usr/bin/env python
# -*- coding: utf-8 -*-

if 'import':
    import os.path
    import sys
    cur_script = __file__
    if sys.platform == 'win32' and sys.version_info.major == 2:
        cur_script = unicode(cur_script, 'cp1251')
    PATH_SCRIPT = os.path.abspath(os.path.dirname(cur_script))
    os.chdir(PATH_SCRIPT)
    # import accessory.colorprint as cp
    # import accessory.clear_consol as cc
    # import accessory.authorship

    from app import app
    from app import db

    from posts.blueprint import posts

    import view


if __name__ == '__main__':
    app.register_blueprint(posts, url_prefix='/blog')
    app.run()


# ------------------------------------------------------------------------------
# ==============================================================================




