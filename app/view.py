#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from app import app
from flask import render_template, send_from_directory


@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                        'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/')
def index():
    name = 'Ivan (Ваня)'
    return render_template('index.html', n=name)

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404
