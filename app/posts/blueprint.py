#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function
from flask import Blueprint
from flask import render_template

from models import Post, Tag
from .forms import PostForm
from app import db
from flask import redirect, url_for, request
from flask_security import login_required
# from sqlalchemy.sql.functions import current_timestamp
# created = db.Column(db.DateTime, default=current_timestamp())

posts = Blueprint('posts', __name__, template_folder='templates')



# http://flask_blog.localhost:5020/blog/create
@posts.route('/create', methods=['POST', 'GET'])
@login_required
def create_post():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        if title:
            try:
                post = Post(title=title, body=body)
                db.session.add(post)
                db.session.commit()
            except:
                print('Something is Wrong')
            return redirect(url_for('posts.index'))

    form = PostForm()
    return render_template('posts/create_post.html', form=form)


@posts.route('/<slug>/edit', methods=['POST', 'GET'])
@login_required
def edit_post(slug):
    post = Post.query.filter(Post.slug==slug).first_or_404()
    if request.method == 'POST':
        form = PostForm(formdata=request.form, obj=post)
        form.populate_obj(post)
        db.session.commit()
        return redirect(url_for('posts.post_detail', slug=post.slug))

    form = PostForm(obj=post)
    return render_template('posts/edit_post.html', post=post, form=form)


@posts.route('/')
def index():
    q = request.args.get('q', u'')

    page = request.args.get('page')
    page = int(page) if page and page.isdigit() else 1

    if q:
        posts = Post.query.filter(Post.title.contains(q) | Post.body.contains(q)) # .all()
    else:
        posts = Post.query.order_by(Post.created.desc())  # .all()

    pages = posts.paginate(page=page, per_page=5)
    return render_template('posts/index.html', pages=pages, q=q)


# http://flask_blog.localhost:5020/blog/first-post
@posts.route('/<slug>')
def post_detail(slug):
    post = Post.query.filter(Post.slug==slug).first_or_404()
    tags = post.tags
    return render_template('posts/post_detail.html', post=post, tags=tags)


# http://flask_blog.localhost:5020/blog/tag/python
@posts.route('/tag/<slug>')
def tag_detail(slug):
    tag = Tag.query.filter(Tag.slug==slug).first_or_404()
    posts = tag.posts # .all()
    return render_template('posts/tag_detail.html', tag=tag, posts=posts)
