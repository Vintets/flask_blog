#!/usr/bin/env python
# -*- coding: utf-8 -*-

from wtforms import Form, StringField, TextAreaField


class PostForm(Form):
    title = StringField('Title')
    body = TextAreaField('Body')
