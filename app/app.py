#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib.parse import urlparse
from flask import Flask
from configs.config import ConfigurationLocal
# from configs.config import Configuration
from flask_sqlalchemy import SQLAlchemy

from flask_migrate import Migrate  #, MigrateCommand
# from flask_script import Manager

from flask_admin import Admin, AdminIndexView
from flask_admin.contrib.sqla import ModelView
from flask_admin.menu import MenuLink  # MenuCategory, MenuView

from flask_security import SQLAlchemyUserDatastore
from flask_security import Security
from flask_security import current_user

from flask import redirect, url_for, request


URL_ADMIN = '/admin'

app = Flask(__name__)
app.config.from_object(ConfigurationLocal)

db = SQLAlchemy(app)


migrate = Migrate(app, db)
# manager = Manager(app)
# manager.add_command('db', MigrateCommand)


### ADMIN ###
from models import *


class AdminMixin:
    def is_accessible(self):
        return current_user.has_role('admin')

    def inaccessible_callback(self, name, **kwargs):
        """Redirect to login page if user doesn't have access"""
        url_next = urlparse(request.url).path
        return redirect(url_for('security.login', next=url_next))


class BaseModelView(ModelView):
    column_display_pk = True  # показывать первичные ключи

    def on_model_change(self, form, model, is_created):
        model.generate_slug()
        return super(BaseModelView, self).on_model_change(form, model, is_created)


class AdminView(AdminMixin, ModelView):
    can_delete = False  # запретить удаление записи
    column_display_pk = True  # показывать первичные ключи
    # form_columns = ('slug',)  # показать только перечисленные поля в форме редактирования/создания
    # form_excluded_columns = ('slug',)  # скрыть перечисленные поля из формы редактирования/создания
    # form_create_rules = {'name', 'posts'}  # разрешённые поля в создании
    # form_edit_rules = ('name', 'posts')  # разрешённые поля в редактировании


# Create customized index view class that handles login & registration
class HomeIndexView(AdminMixin, AdminIndexView):
    pass


class PostAdminView(AdminMixin, BaseModelView):
    form_columns = ('title', 'body', 'tags')


class TagAdminView(AdminMixin, BaseModelView):
    form_columns = ('name',)


admin = Admin(app,
              name='FlaskApp',
              # template_mode='bootstrap3',
              url='/',
              index_view=HomeIndexView(name='Home Admin'),  # , url=URL_ADMIN
              # base_template='admin/my_master.html'
              )
admin.add_view(AdminView(AppUser, db.session))
admin.add_view(PostAdminView(Post, db.session))
admin.add_view(TagAdminView(Tag, db.session))
admin.add_link(MenuLink(name='На главную', url='/', target='_blank', category=''))


### Flask_security ###
user_datastore = SQLAlchemyUserDatastore(db, AppUser, Role)
security = Security(app, user_datastore)


# flask shell   запуск интерпритатора с предварительными импортами
@app.shell_context_processor
def make_shell_context():
    # from models import Post, Tag
    return {'db': db,
            'Post': Post,
            'Tag': Tag,
            'user_datastore': user_datastore,
            }


# ------------------------------------------------------------------------------
# ==============================================================================
