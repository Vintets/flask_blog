#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import db
from datetime import datetime
from accessory.translit import translit
import re
from sqlalchemy.sql.functions import current_timestamp
from flask_security import UserMixin, RoleMixin


def slugify(s):
    translit_s = translit(s)
    pattern = r'[^\w+]'
    format_str = re.sub(pattern, '-', translit_s.lower())
    while '--' in format_str:
        format_str = format_str.replace('--', '-')
    return format_str

post_tags = db.Table('post_tags',
            db.Column('post_id', db.Integer, db.ForeignKey('post.id')),
            db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'))
            )


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(140))
    slug = db.Column(db.String(140), unique=True)
    body = db.Column(db.Text)
    # created = db.Column(db.DateTime, default=datetime.now())
    created = db.Column(db.DateTime, default=current_timestamp())

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        self.generate_slug()

    tags = db.relationship('Tag', secondary=post_tags, backref=db.backref('posts', lazy='dynamic'))

    def generate_slug(self):
        if self.title:
            self.slug = slugify(self.title)

    def __str__(self):
        return f'{self.title}'

    def __repr__(self):
        return f'<Post id: {self.id}, slug: {self.slug}>'


class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    slug = db.Column(db.String(100), unique=True)

    def __init__(self, *args, **kwargs):
        super(Tag, self).__init__(*args, **kwargs)
        self.generate_slug()

    def generate_slug(self):
        self.slug = slugify(self.name)

    def __repr__(self):
        # return f'<Tag id: {self.id}, name: {self.name}>'
        return f'{self.name}'


### Flask Security ###

roles_users = db.Table('roles_users',
            db.Column('user_id', db.Integer, db.ForeignKey('app_user.id')),
            db.Column('role_id', db.Integer, db.ForeignKey('role.id'))
            )

class AppUser(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean())
    roles = db.relationship('Role', secondary=roles_users, backref=db.backref('users', lazy='dynamic'))

    # Flask-Login integration
    # NOTE: is_authenticated, is_active, and is_anonymous
    # are methods in Flask-Login < 0.3.0
    # @property
    # def is_authenticated(self):
    #     return True

    # @property
    # def is_active(self):
    #     return bool(self.active)

    # @property
    # def is_anonymous(self):
    #     return False

    # def get_id(self):
    #     return self.id

    def __repr__(self):
        return f'<AppUser {self.email}>'


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    description = db.Column(db.String(255))

    def __repr__(self):
        return f'{self.name}'


# ------------------------------------------------------------------------------


'''
# создание юзера через консоль
from app import db
from app import user_datastore
user_datastore.create_user(email='oleg@test.ru', password='admin')
db.session.commit()
'''


'''
# создание роли
from app import db
from app import AppUser
from app import user_datastore
user = AppUser.query.first()
user
user_datastore.create_role(name='admin', description='administrator')
db.session.commit()

# назначаем роль юзеру
from models import Role
role = Role.query.first()
user_datastore.add_role_to_user(user, role)
db.session.commit()
'''


'''
import models
from app import db
from models import Post
db.create_all()
p = Post(title='First post', body='First post body')
db.session.add(p)
db.session.commit()

p
p.slug

p1 = Post(title='Second post', body='Second post body')
db.session.add(p1)
db.session.commit()
p1
p1.created

p2 = Post(title='Third post! 3-test', body='Third post body')
db.session.add(p2)
db.session.commit()
p2.slug


p3 = Post(title=u'Четвёртый пост! 4-test', body=u'Много текста в 4-м посту')
db.session.add(p3)
db.session.commit()
p3.slug


Можно добавить в базу сразу список
db.session.add_all([p1,p2,p3])

# список всех постов
posts = Post.query.all()

# поиск поста по заголовку
содержит
p2 = Post.query.filter(Post.title.contains('second')).all()
равен
p3 = Post.query.filter(Post.title=='Third post! 3-test').all()


---------------------

from app import db
from models import Post, Tag
tag = Tag(name='python')
db.session.add(tag)
db.session.commit()

# взять первый тег
t = Tag.query.first()

>>> post1 = Post.query.filter(Post.id==1)
>>> post1
<flask_sqlalchemy.BaseQuery object at 0x0000000004E9F2B0>
# количество отобранных объектов
>>> post1.count()
1
>>> post1 = post1.first()
>>> post1
<Post id: 1, slug: First-post>

# появилось новое свойство - список тегов
post1.tags
post1.tags.append(t)

# и появился ответный
t.posts.count()

# сохраняем
db.session.add(post1)
db.session.commit()

'''





