#!/usr/bin/env python
# -*- coding: utf-8 -*-


from __future__ import print_function
import os.path
import time
from accessory.ini import Ini


class ConfigIni(object):
    def __init__(self):
        ini_auth_name   = os.path.join('configs', 'auth_code.ini')
        self.ini_auth   = Ini(ini_auth_name)

    def _err_config_ini(self, err_text):
        print(err_text)
        time.sleep(3)
        exit()

    def get_mysql_authorization(self, section=''):
        if not section:
            self._err_config_ini(u'Не указана секция в ini')
        if not self.ini_auth.set_name_section(section):
            self._err_config_ini(u'Не найдена секция [%s] в ini' % section)
        return{
                u'user': self.ini_auth.get_param('user'),
                u'pass': self.ini_auth.get_param('pass'),
                u'host': self.ini_auth.get_param('host'),
                u'base_name': self.ini_auth.get_param('base_name')
                }

    def get_mysql_authorization_SQLAlchemy(self, section=''):
        _auth = self.get_mysql_authorization(section=section)
        return u'mysql+mysqlconnector://%(u)s:%(p)s@%(h)s/%(b)s' % {
                    'u': _auth[u'user'],
                    'p': _auth[u'pass'],
                    'h': _auth[u'host'],
                    'b': _auth[u'base_name'],
                    }


class ConfigurationLocal(object):
    DEBUG = True
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = ConfigIni().get_mysql_authorization_SQLAlchemy(section='mysql_local')
    # SQLALCHEMY_DATABASE_URI = 'sqlite:///flask_blog.db'
    SECRET_KEY = 'something very secret'
    SQLALCHEMY_NATIVE_UNICODE = 'utf-8'
    STATIC_FOLDER = 'static'
    SERVER_NAME = 'flask_blog.localhost:5020'

    ### flask-security
    SECURITY_PASSWORD_SALT = 'salt'
    SECURITY_PASSWORD_HASH = 'bcrypt'


# class Configuration(object):
    # DEBUG = True
    # TEMPLATES_AUTO_RELOAD = True
    # SQLALCHEMY_TRACK_MODIFICATIONS = False
    # SQLALCHEMY_DATABASE_URI = ConfigIni().get_mysql_authorization_SQLAlchemy(section='mysql')
    # SQLALCHEMY_NATIVE_UNICODE = 'utf-8'
    # STATIC_FOLDER = 'static'


# ------------------------------------------------------------------------------
# ==============================================================================

# реализуют паттерн MVC
# Model   db    описание модели в БД
# View          отображение пользователю при помощи шаблонов
# Controller    связь между пользователем и БД, обработка запросов

