#!/usr/bin/env python
# -*- coding: utf-8 -*-

from app import migrate, manager
from main import *


if __name__ == '__main__':
    manager.run()


# new version
'''
flask-script больше не поддерживается. Не устанавливайте его, вместо этого вам стоит:
1) Из трех строк 
migrate = Migrate(app, db)

manager = Manager(app)

manager.add_command('db', MigrateCommand)
оставьте только первую
2) Создавать файл manage.py не нужно, миграции создаются командой 
$ flask db init
# flask db migrate

У flask теперь есть свой менеджер для этих команд, поэтому вам не нужно создавать его вручную
'''


# ------------------------------------------------------------------------------
# ==============================================================================


# new version
# инициализация
# flask db init

# создать миграцию
# flask db migrate

# применить миграцию
# flask db upgrade



#---------------------------------
# инициализация
# python manage.py db init

# создать миграцию
# python manage.py db migrate

# применить миграцию
# python manage.py db upgrade



#---------------------------------
# Отмена сломанной миграции
# 1. выяснить, какая часть миграции действительно была выполнена
# 2. изменить downgrade (), чтобы отменить только те изменения, которые были применены, удалить все остальное.
# 3. сделать upgrade() пустой функцией с pass
# 4. снова запустить
    # python manage.py db upgrade
    # чтобы заставить Alembic выполнить теперь пустую миграцию.
# 5. запустить
    # python manage.py db downgrade
    # чтобы отменить частичные изменения


# посмотреть только заголовок ???
# чтобы пометить базу данных как обновленную
# python manage.py db stamp head


# в env.py файл, можно указать существующие таблицы, и он не будет создавать эти таблицы :
# metadata.reflect(engine, only=["table1", "table2"])


'''
добавить include_schemas=True
context.configure(
    connection=connection,
    target_metadata=target_metadata,
    include_schemas=True,
    process_revision_directives=process_revision_directives,
'''

# поставить старую версию
# pip3 install flask-migrate==2.5.3 -U
# pip3 install flask-migrate==2.6.0 -U
